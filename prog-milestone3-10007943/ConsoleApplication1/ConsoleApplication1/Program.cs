﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class customer
    {
        public string name;
        public int number;

        public customer()
        {
            name = "";
            number = 0;
        }
        public customer(string nme, int phone)
        {
            nme = name;
            number = phone;
        }
        public void print()
        {
            Console.WriteLine("Confirm your details ?");
            Console.WriteLine($" Your name: {name}");
            Console.WriteLine($" Your contact number: {number}");
        }
    }
    class pizzas
    {
        public class Hotnspicysmall
        {
            public string type = "Hot n spicy";
            public string size = "small";
            public double price = 7.50;
        }
        public class Hotnspicymedium
        {
            public string type = "Hot n spicy";
            public string size = "Medium";
            public double price = 11.50;
        }
        public class Hotnspicylarge
        {
            public string type = "Hot n spicy";
            public string size = "Large";
            public double price = 14.00;
        }
        public class Tropicalvegsmall
        {
            public string type = "Tropical veg";
            public string size = "Small";
            public double price = 7.50;
        }
        public class Tropicalvegmedium
        {
            public string type = "Tropical veg";
            public string size = "Medium";
            public double price = 11.50;
        }
        public class Tropicalveglarge
        {
            public string type = "Tropical veg";
            public string size = "Large";
            public double price = 14.00;
        }
        public class Pepporonismall
        {
            public string type = "Pepporoni";
            public string size = "Small";
            public double price = 7.50;
        }
        public class Pepporonimedium
        {
            public string type = "Pepporoni";
            public string size = "Medium";
            public double price = 11.50;
        }
        public class Pepporonilarge
        {
            public string type = "Pepporoni";
            public string size = "Large";
            public double price = 14.00;
        }
        public class Chickensupremesmall
        {
            public string type = "Chicken supreme";
            public string size = "Small";
            public double price = 7.50;
        }
        public class Chickensuprememedium
        {
            public string type = "Chicken supreme";
            public string size = "Medium";
            public double price = 11.50;
        }
        public class Chickensupremelarge
        {
            public string type = "Chicken supreme";
            public string size = "Large";
            public double price = 14.00;
        }
    }
    class Program
    {
        static Dictionary<string, double> dict = new Dictionary<string, double>();
        static void Main(string[] args)
        {
            Details();
        }
        static void Details()
        {
            string name;
            var number = 0;
            Console.WriteLine($"              *********          ");
            Console.WriteLine($"          ***           ***      ");
            Console.WriteLine($"        **                 **    ");
            Console.WriteLine($"      **                     **  ");
            Console.WriteLine($"     **                       ** ");
            Console.WriteLine($"    **                         **");
            Console.WriteLine($"    *           PIZZA           *");
            Console.WriteLine($"    *           MENU            *");
            Console.WriteLine($"    *                           *");
            Console.WriteLine($"    **                         **");
            Console.WriteLine($"     **                       ** ");
            Console.WriteLine($"      **                     **  ");
            Console.WriteLine($"         **                 **   ");
            Console.WriteLine($"           ***           ***     ");
            Console.WriteLine($"               ********          ");

            Console.WriteLine($"Please enter you name");
            name = Console.ReadLine();

            Console.WriteLine($"Please enter your contact number");
            number = int.Parse(Console.ReadLine());
            Menu();
        }
        static void Menu()
        {
            int choice = 0;
            Console.WriteLine($"----------------------------");
            Console.WriteLine($"**** 1. Pizza Selection ****");
            Console.WriteLine($"**** 2. Drink Selection ****");
            Console.WriteLine($"**** 3. Finish and PAY *****");
            Console.WriteLine($"----------------------------");
            Console.WriteLine($"  Please select any option  ");
            var ans = (Console.ReadLine());
            if (int.TryParse(ans, out choice))
            {
                switch (choice)
                {
                    case 1:
                        pizzamenu();
                        break;
                    case 2:
                        drinks();
                        break;
                    case 3:
                        paymentmenu();
                        break;
                }

            }
        }
        static void pizzamenu()
        {
            var hotnspicy = "Hot n spicy";
            var Tropicalveg = "Tropical veg";
            var Pepporoni = "Pepporoni";
            var Chickensupreme = "Chicken supreme";
            Console.WriteLine($"------------------PIZZA---------------------");
            Console.WriteLine($"------------- 1.Hot n spicy-----------------");
            Console.WriteLine($"------------- 2.Tropical veg ---------------");
            Console.WriteLine($"------------- 3.Pepporoni ------------------");
            Console.WriteLine($"------------- 4.Chicken supreme ------------");
            Console.WriteLine($"--------------------------------------------");
            Console.WriteLine($"-----------Please select any one------------");

            var selection = Console.ReadLine();
            switch (selection)
            {
                case "1":
                    Console.WriteLine("Your {0} pizza is getting ready \n", hotnspicy);
                    pizzasize(hotnspicy);
                    break;

                case "2":
                    Console.WriteLine("Your {0} pizza is getting ready \n", Tropicalveg);
                    pizzasize(Tropicalveg);
                    break;

                case "3":
                    Console.WriteLine("Your {0} pizza is getting ready \n", Pepporoni);
                    pizzasize(Pepporoni);
                    break;

                case "4":
                    Console.WriteLine("Your {0} pizza is getting ready \n", Chickensupreme);
                    pizzasize(Chickensupreme);
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please try again");
                    break;
            }
        }
        static void pizzasize(string pizza)
        {

            Console.WriteLine($" What size {pizza} pizza would you like?");
            double small = 7.50;
            double medium = 11.50;
            double large = 14.00;

            Console.WriteLine($"Cost for small size:  ${small} \n");
            Console.WriteLine($"Cost for medium size: ${medium} \n");
            Console.WriteLine($"Cost for large size:  ${large} \n");

            var size = Console.ReadLine();
            switch (size)
            {
                case "small":
                    Console.WriteLine($"You have selected one small {pizza} pizza. \n Your total cost is ${small}");
                    dict.Add(pizza, small);
                    break;

                case "medium":
                    Console.WriteLine($"You have selected one medium {pizza}.\n Your total cost is ${medium} ");
                    dict.Add(pizza, medium);
                    break;

                case "large":
                    Console.WriteLine($"You have selected one large {pizza}.\n Your total cost is ${large} ");
                    dict.Add(pizza, large);
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please try agian.");
                    break;
            }
            drinks();
        }
        static void paymentmenu()
        {
            var totalamount = dict.Sum(x => x.Value);

            Console.WriteLine($" Your order total is: ${totalamount}");

            Console.WriteLine("Please make your payment");
            var money = double.Parse(Console.ReadLine());

            if (money >= totalamount)
            {
                Console.WriteLine($"Your change is ${money - totalamount} \n Thank you. Have a good day");
                Console.ReadLine();
            }
            else if (money <= totalamount)
            {
                Console.WriteLine($"Sorry. you need more ${totalamount - money} money");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Invalid entry, Try agian");
            }
        }
        static void drinks()
        {

            Console.WriteLine($"\n Please choose a drink that you like to have?");

            double raspberry = 2.50;
            double coke = 4.00;
            double sprite = 3.00;
            double Vdrink = 3.50;

            Console.WriteLine("********Drinks*******");
            Console.WriteLine($" Raspberry: ${raspberry}");
            Console.WriteLine($" Coke:${coke}");
            Console.WriteLine($" Sprite: ${sprite}");
            Console.WriteLine($" Vdrink: ${Vdrink}");
            Console.WriteLine($"********************");

            var drink = Console.ReadLine();
            switch (drink)
            {
                case "Raspberry":
                    Console.WriteLine($"\n Cost of Raspberry is: ${raspberry}");
                    dict.Add(drink, raspberry);
                    break;

                case "coke":
                    Console.WriteLine($"\n Cost of coke is: ${coke}");
                    dict.Add(drink, coke);
                    break;

                case "Fanta":
                    Console.WriteLine($"\n Cost of sprite is: ${sprite} ");
                    dict.Add(drink, sprite);
                    break;

                case "Vdrink":
                    Console.WriteLine($"\n Cost of Vdrink is: ${Vdrink} ");
                    dict.Add(drink, Vdrink);
                    break;

                default:
                    Console.WriteLine("Invalid selection");
                    dict.Clear();
                    break;
            }
            paymentmenu();
        }
    }
}